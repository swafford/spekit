#!/usr/bin/env python

#Ver. 0.2.1

from itertools import islice
from pandas import *
import numpy as np
import matplotlib
matplotlib.use('gtkAgg')
import matplotlib.pyplot as plt
from pylab import *
from math import log
import os
import multiprocessing
from scipy import stats
import time
from multiprocessing import Process, Pipe
import sys
import gobject
from matplotlib.ticker import ScalarFormatter, FormatStrFormatter
from decimal import *
import os, errno
import argparse
import ntpath
import pipes
import pygtk
import gtk
import copy

class spekit(object):
    def __init__(self,o,i,winrange,quick,auto,noplot,nosave,skiptocol,hardcap):
        self.examine = False
        self.editing = False
        self.plotopen = False
        self.hardcap = hardcap
        self.wrange = winrange
        self.edit_defaults = {"1":['Cap','Unlocked',0,'No Cap'],
                            "2":['Anchor','Unlocked',False,'No Anchor'],
                            "3":['Resort','Unlocked',False,'Slope of Ln'],
                            "4":['Window','Unlocked',winrange,winrange[0]],
                            "5":['Manual','Unlocked',False,'False']}
        self.set_edits = copy.deepcopy(self.edit_defaults)
        self.bypasslevel = [quick,auto,noplot,nosave]
        self.data_input = i
        self.date_stamp = time.strftime("%d_%m_%Y")
        if skiptocol != 0:
            self.colnum = skiptocol - 1
            self.i = skiptocol - 1
        else:
            self.colnum = 0
            self.i = 0
        self.xx = DataFrame.from_csv(self.data_input, sep =',')
        self.plot_pipe, self.plotter_pipe = Pipe()
        self.send = self.plot_pipe.send
        self.final_dict = {}
        self.wrange = winrange
        self.cap = self.hardcap
        if o == None:
            self.out_dir = os.path.normpath(os.path.join(os.path.abspath(self.data_input),'..','spekit_{0}'.format(os.path.splitext(ntpath.basename(i))[0])))
        else:
            self.out_dir = os.path.normpath(os.path.join(o,'spekit_{0}'.format(os.path.splitext(ntpath.basename(i))[0])))
        if os.path.exists(self.out_dir):
            i=0
            while os.path.exists("{0}_{1}".format(self.out_dir,i))==True:
                i+=1
            if not os.path.exists("{0}_{1}".format(self.out_dir,i)):
                self.out_dir = "{0}_{1}".format(self.out_dir,i)
                self.mkdir_p(self.out_dir)  
        else:
            self.mkdir_p(self.out_dir)
        self.out_file = os.path.join(self.out_dir,"Full_output.csv")
        x = open(self.out_file,'wb')
        x.write("Run Name,Specific Growth Rate,Lag Time, Window Size, StartX, StartY, StopX, StopY, Slope Equation (non-log), Slope Equation(Ln), Choice Selected\n")
        x.close()
        x = open(os.path.join(self.out_dir,"COMMANDS.txt"),'w')
        x.write("python ")
        for y in sys.argv:
            x.write("{0} ".format(y))
            
        x.close()
        self.reset()
        self.start()
        
    def start(self):
        while self.i < len(self.xx.columns):
            self.cap = self.set_edits['1'][2]
            self.plotopen = False
            run_name = self.xx.columns[self.colnum]
            self.save_file = os.path.join(self.out_dir,run_name)
            self.save_file = os.path.normpath(self.save_file)
            self.temp_choice = None
            self.winnum = 0
            self.logtime = False
            self.winList = []
            self.windex = {}
            self.unlogwindex = {}
            self.windowDict = {}
            self.hiddenwindex = {}
            self.intDict = {}
            self.winrange = self.set_edits['4'][2][0]
            self.winmax = self.set_edits['4'][2][1]
            print("\nYou are currently analyzing column #{0} of {1} columns. ({2})".format(self.colnum+1,len(self.xx.columns),self.xx.columns[self.colnum]))
            txt = self.set_edits
            print("\nSorting by '{0}' | Anchor: '{1}' | Cap: '{2}' | MinWin: '{3}' | Manual '{4}'".format(txt['3'][3],
                                                                                                        txt['2'][3],
                                                                                                        txt['1'][3],
                                                                                                        txt['4'][3],
                                                                                                        txt['5'][3]))
            if self.set_edits['5'][2]:
                self.set_edits['5'][2] = list(self.find_anchor_head(self.set_edits['5'][2][0],self.set_edits['5'][2][1]))
            if self.set_edits['2'][2]:
                self.set_edits['2'][2] = self.find_anchor_head(self.set_edits['2'][2])
            while self.winrange <= self.winmax:
                self.analyze_window(self.windowed(n=self.winrange))
                self.winrange+=1
            choice_fragment = open("{0}_Choices.csv".format(self.save_file), 'wb')
            choice_fragment.write("{0},{1},{2},{3},{4}\n".format("Choice","R2","Slope","Win Start","Win End"))
            cx = 1
            for wi in self.choiceDF.index[:20]:
                if cx <= 4:
                    self.winList.append(self.windowDict[wi])
                    cx += 1
                choice_fragment.write("{0},{1},{2},{3},{4}\n".format(wi,
                                                                     self.choiceDF["R2"][wi],
                                                                     self.choiceDF["Slope"][wi],
                                                                     min(self.windowDict[wi]),
                                                                     max(self.windowDict[wi])))
            choice_fragment.close()
            print ("Plotting..")
            if self.set_edits['5'][2] != False:
                print self.choiceDF.ix[self.manual_selection]
                self.temp_choice = self.manual_selection
                self.logtime= True
                self.plot_type = 'single'
                self.plot_prep()
                
                self.singlechoice_final()
            elif len(self.choiceDF.index) < 4 and self.editing == True:
                print ("\nResult list too small to graph. Please examine individually\n")
                self.logtime = True
                self.singlechoice()
            else:
                self.plot_quad()
                self.choose()
            self.save_vars()
            
    def save_vars(self):
        if self.i < len(self.xx.columns):
            self.colnum += 1
            with open(self.out_file,'a') as outfile:
                for key in self.final_dict:
                    outfile.write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\n".format(key,
                                                                                 self.final_dict[key][0],
                                                                               self.final_dict[key][1],
                                                                               self.final_dict[key][2],
                                                                               self.final_dict[key][3],
                                                                               self.final_dict[key][4],
                                                                               self.final_dict[key][5],
                                                                               self.final_dict[key][6],
                                                                               self.final_dict[key][7],
                                                                                     self.final_dict[key][8],
                                                                                          self.final_dict[key][9]))
            self.final_dict = {}
            if self.bypasslevel[3] == False:
                print("Saving..")
                time.sleep(3)
                print("Saved!")
            print("\n##############################\n")
            self.i += 1
            for a in self.set_edits.keys():
                if self.set_edits[a][1] == 'Unlocked':
                    self.set_edits[a] = copy.deepcopy(self.edit_defaults[a])
            time.sleep(1)
            self.start()

    def find_anchor_head(self,anchor,head=False):
        win_min_dict = {}
        win_max_dict = {}
        for x in self.xx.index:
            win_min_dict.setdefault(abs(x-anchor),x)
            if head:
                win_max_dict.setdefault(abs(x-head),x)
        if head:
            return(win_min_dict[min(win_min_dict)],win_max_dict[min(win_max_dict)])
        else:
            return(win_min_dict[min(win_min_dict)])
        
    def plot_quad(self):
        self.plot_type = 'quad'
        self.logtime = True
        self.plot_prep()
        
        
    def reset(self):
        print "resetting"
        if self.editing == False:
            print "inside"
            self.cap = self.hardcap
            self.anchor_num = False
            self.resort_num = False
            self.manual_num = False
            self.set_window_num = False
            self.set_window_txt = self.wrange[0]
            self.manual_txt = "False"
            self.cap_txt = "No cap"
            self.anchor_txt = "No Anchor"
            self.resort_txt = "Slope of Ln"
            print "done setting"
    
    def choose(self):
        if len(self.choiceDF.index) > 8:
            print(self.choiceDF[0:8])
        else:
            print(self.choiceDF)
        if self.bypasslevel[1] == True:
            self.temp_choice = self.winList[0][0]
            self.singlechoice_final()
        else:
            self.choice = raw_input("Which set would you like to keep?\nTo view more sets or enter Editing mode enter '-'.\nspekit> ")
            if self.choice == '-':
                self.examine = True
                self.edit_mode()
            else:
                try:
                    self.examine = False
                    self.temp_choice = int(self.choice)
                    self.close_graph()
                    
                    self.plot_type = 'single'
                    self.plot_prep()
                    
                    self.singlechoice_final()
                except:
                    print("You have made an invalid selection.")
                    self.choose()
                

    def plot_prep(self,finished=False):
        if self.bypasslevel[3] != True and self.plotopen==False:
                self.data = []
                self.plotter = ProcessPlotter()
                self.plot_process = Process(target = self.plotter,args = (self.plotter_pipe,
                                                                          self.plot_type,
                                                                          self.lnDF,
                                                                          self.winList,
                                                                          self.colnum,
                                                                          self.windowDict,
                                                                          self.temp_choice,
                                                                          self.choiceDF,
                                                                          self.logtime,
                                                                          self.xx,
                                                                          self.intDict,
                                                                          self.save_file,
                                                                          self.bypasslevel,
                                                                          self.examine))
                self.plot_process.daemon = True
                self.plotopen = True
                self.plot_process.start()

    def edit_mode(self):
        self.editing = True
        tlist = []
        for x in self.set_edits.keys():
            if x == '4':
                if self.set_edits[x][1] == 'Locked':
                    tlist.append("{0}*".format(self.set_edits[x][2]))
                else:
                    tlist.append(self.set_edits[x][2])
                continue
            if self.set_edits[x][1] == 'Locked':
                tlist.append("{0}*".format(self.set_edits[x][3]))
            else:
                tlist.append(self.set_edits[x][3])
        print("\nCurrent Edits:")
        print("Win: {0} | Anchor: {1} | Cap: {2} | Sort: {3} | Manual: {4}".format(tlist[4],
                                                                                   tlist[2],
                                                                                   tlist[0],
                                                                                   tlist[1],
                                                                                   tlist[3]))
        self.e_mode_choice = raw_input("\nPlease select a command from the following list:\
\n1. Single: View and select a single window from an expanded list.\
\n2. Cap:    Exclude windows with time points higher than a certain value.\
\n3. Anchor: Only consider windows with time points starting at a certain value.\
\n4. Resort: Re-sort the windows based on a different criteria [R2 | Slope].\
\n5. Window: Set the Max/Min window size.\
\n6. Manual: Manually select window.\
\n7. Lock/Unlock: Lock and unlock edits.\
\n8. Clear: Clear all or some of your current edits.\
\n9. Start: Re-run with the current edits.\
\nspekit> ")
        try:
            self.e_mode_choice = int(self.e_mode_choice)
        except:
            print("\nERROR: Invalid selection. Please use a single, whole number.\n")
            self.edit_mode()
        if self.e_mode_choice == 1:
            self.singlechoice()
            self.save_vars()
        elif self.e_mode_choice == 2:
            self.cap_func()
            self.edit_mode()
        elif self.e_mode_choice == 3 :
            self.anchor()
            self.edit_mode()
        elif self.e_mode_choice == 4 :
            self.resort()
            self.edit_mode()
        elif self.e_mode_choice == 5:
            self.set_window()
            self.edit_mode()
        elif self.e_mode_choice == 6:
            self.manual()
            self.edit_mode()
        elif self.e_mode_choice == 7:
            self.lock_edits()
            self.edit_mode()
        elif self.e_mode_choice == 8:
            self.clear_edits()
            self.edit_mode()
        elif self.e_mode_choice == 9:
            self.close_graph()
            self.start()
        else:
            print("\nERROR: Invalid selection, Please use a selection provided.\n")
            self.edit_mode()
    
    def clear_edits(self):
        print("\nSelect the number of the edits to clear. Separate multiple edits with commas, no spaces.")
        clear_input = raw_input("1. Cap\n2. Anchor\n3. Resort\n4. Window\n5. Manual\n6. All\nspekit> ")
        clear_list = clear_input.split(",")
        for a in clear_list:
            if a == '6':
                for var in self.set_edits.keys():
                    if self.set_edits[var][2] != self.edit_defaults[var][2] or self.set_edits[var][3] != self.edit_defaults[var][3]:
                        self.set_edits[var] = copy.deepcopy(self.edit_defaults[var])
                print("Cleared all.")
                time.sleep(1)
                continue
            if a == '5':
                    self.set_edits['1'] = copy.deepcopy(self.edit_defaults['1'])
                    self.set_edits['2'] = copy.deepcopy(self.edit_defaults['2'])
            if self.set_edits[a][2] != self.edit_defaults[a][2]:
                print self.set_edits[a]
                print self.edit_defaults[a]
                self.set_edits[a] = copy.deepcopy(self.edit_defaults[a])
                print("Cleared {0}.".format(self.set_edits[a][0]))
                time.sleep(0.5)
                
    def cap_func(self):
        try:
            x = float(raw_input("\nValue to cap search at:  "))
        except:
            print("\nERROR:Invalid entry, please use a number, Decimals are ok.\n")
            self.edit_mode()
        self.cap = x
        self.set_edits[str(self.e_mode_choice-1)][2] = self.cap
        self.set_edits[str(self.e_mode_choice-1)][3] = self.cap
            
    def anchor(self):
        try:
            x = float(raw_input("\nValue to anchor search at:  "))
        except:
            print("\nERROR:Invalid entry, please use a number, Decimals are ok.\n")
            self.edit_mode()
        self.set_edits[str(self.e_mode_choice-1)][2] = x
        self.set_edits[str(self.e_mode_choice-1)][3] = x
            
    def manual(self):
        tman = []
        try:
            tman.append(float(raw_input("\nSet MINimum time point: ")))
            tman.append(float(raw_input("Set MAXimum time point: ")))
        except:
            print("\nERROR:Invalid entry, please use a number, Decimals are ok.\n")
            self.edit_mode()
        x = tman[1]-tman[0]
        if x <= 2:
            print("\nYour Max/Min are too close together!\n")
            self.edit_mode()
        else:
            self.set_edits[str(self.e_mode_choice-1)][2] = tman
            self.set_edits[str(self.e_mode_choice-1)][3] = 'True'
            self.set_edits['1'][3] = '{0}*'.format(tman[1])
            self.set_edits['2'][3] = '{0}'.format(tman[0])
    
    def resort(self):
        print("\nSelect value to sort by:")
        print("1. R2")
        print("2. Slope of Ln")
        x = raw_input("spekit> ")
        if not x in ['1','2']:
            print("\nERROR: Please use the numbered selection [1 or 2]!\n")
            self.edit_mode()
        else:
            self.set_edits['3'][2] = x
            if x == '1':
                self.set_edits['3'][3] = 'R2'
            else:
                self.set_edits['3'][3] = 'Slope of Ln'
                
    def set_window(self):
        x = []
        try:
            x.append(int(raw_input("\nSet MINimum window size: ")))
            x.append(int(raw_input("Set MAXimum window size: ")))
        except:
            print("\nERROR:Invalid entry, please use whole numbers\n")
            self.edit_mode()
        dist = x[1]-x[0]
        if dist < 0:
            print("\nYour Window Min is greater than your Window Max.")
            self.edit_mode()
        else:
            self.set_edits['4'][2] = x
            self.set_edits['4'][3] = x[0]
    
    def lock_edits(self):
        print("\nSelect the number of the edits to lock/unlock. Separate multiple edits with commas, no spaces.")
        lock_input = raw_input("1. Cap  [{0}]\n\
2. Anchor  [{1}]\n\
3. Resort  [{2}]\n\
4. Window  [{3}]\n\
5. Lock All\n\
6. Unlock All\n\
spekit> ".format(self.set_edits['1'][1],
                 self.set_edits['2'][1],
                 self.set_edits['3'][1],
                 self.set_edits['4'][1]))
        lock_list = lock_input.split(",")
        for a in lock_list:
            if a == '-':
                self.edit_mode()
            if a == '6':
                for var in self.set_edits.keys():
                    if self.set_edits[var][1] != 'Unlocked' and var != '5':
                        self.set_edits[var][1] = 'Unlocked'
                print("Unlocked all.")
                time.sleep(1)
                continue
            if a == '5':
                for var in self.set_edits.keys():
                    if self.set_edits[var][1] != 'Locked' and var != '5':
                        self.set_edits[var][1] = 'Locked'
                print("Locked all.")
                time.sleep(1)
                continue
            if self.set_edits[a][1] == 'Unlocked':
                self.set_edits[a][1] = 'Locked'
                print("Locked {0}.".format(self.set_edits[a][0]))
                time.sleep(0.5)
            else:
                self.set_edits[a][1] = 'Unlocked'
                print("Unlocked {0}.".format(self.set_edits[a][0]))
                time.sleep(0.5)
                
    def singlechoice(self):
        if len(self.choiceDF.index) > 4:
            self.plot_quad()
        if len(self.choiceDF.index) > 8:
            print(self.choiceDF[0:15])
        else:
            print(self.choiceDF)
        try:
            df_draw = raw_input("Enter a choice from the list to see it graphed. To enter Editing mode, type '-':\nspekit> ")
            if df_draw == '-':
                self.edit_mode()
            self.close_graph()
            
            self.temp_choice = int(df_draw)
            self.plot_type = 'single'
            self.plot_prep()
            
            self.singlechoice_final()
        except:
            print("You have made an invalid selection")
            time.sleep(1)
            self.singlechoice()
            
    def singlechoice_final(self):
        if self.bypasslevel[1] == True or self.bypasslevel[0] == True and self.examine == False:
            df_choose = 'y'
        else:
            df_choose = raw_input("To accept/reject the selection, type 'yes/no'. To enter Editing mode, type '-':\nspekit>  ")
            self.close_graph()
        if df_choose in ['y','Y','yes','Yes']:
            final_data = [self.windex[self.temp_choice][1],
                          self.unlogwindex[self.temp_choice][2],
                          len(self.windowDict[self.temp_choice]),
                          min(self.windowDict[self.temp_choice]),
                          self.xx.loc[min(self.windowDict[self.temp_choice])][self.xx.columns[self.colnum]],
                          max(self.windowDict[self.temp_choice]),
                          self.xx.loc[max(self.windowDict[self.temp_choice])][self.xx.columns[self.colnum]],
                          "y={0}x+{1}".format(self.unlogwindex[self.temp_choice][0],self.unlogwindex[self.temp_choice][1]),
                          "y={0}x+{1}".format(self.hiddenwindex[self.temp_choice][0],self.hiddenwindex[self.temp_choice][1]),
                          self.temp_choice]
            self.logtime = False
            self.final_dict.setdefault(self.xx.columns[self.colnum],final_data)
            self.plot_type = 'final'
            self.plot_prep()
        elif df_choose in ['n','N','no','No']:
            print("resetting selection")
            self.close_graph()
            self.singlechoice()
        elif df_choose == '-':
            self.close_graph()
            if len(self.choiceDF.index) > 3:
                self.plot_quad()
            self.edit_mode()
        else:
            print("Please enter 'y' or 'n'")
            time.sleep(0.2)
            self.singlechoice_final()
            
    def windowed(self,n=8):
        "Returns a sliding window (of width n) over data from the iterable"
        "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
        #have it slice index, then pass the generator into analyze_window
        it = iter(self.xx.index)
        self.result = tuple(islice(it, n))
        if len(self.result) == n:
            yield self.result    
        for elem in it:
            self.result = self.result[1:] + (elem,)
            yield self.result
            
    def save_window(self,window):
            self.windowDict.setdefault(self.winnum,window)
            #yax = self.xx[self.xx.columns[self.colnum]][min(window)-1:max(window)]
            yax = self.xx.ix[window,self.colnum]
            xax = yax.index
            slope, intercept, r_value, p_value, std_err = stats.linregress(xax, np.log(yax))
            self.windex.setdefault(self.winnum,[r_value**2, slope,len(window),window[0],window[-1]])
            self.hiddenwindex.setdefault(self.winnum,[slope, intercept])
            x = (intercept*-1)/slope
            self.intDict.setdefault(self.winnum,[x])
            self.intDict.setdefault(self.winnum,).append([slope,r_value**2,intercept])
            slope, intercept, r_value, p_value, std_err = stats.linregress(xax, yax)
            x = (intercept*-1)/slope
            self.unlogwindex.setdefault(self.winnum,[slope,intercept,x,window])
            self.intDict.setdefault(self.winnum,).append(x)
            self.intDict.setdefault(self.winnum,).append([slope,r_value**2,intercept])
            self.winnum +=1
            
    def analyze_window(self,generator):
        list_of_windows = list(generator)
        for window in list_of_windows:
                if min(window) == self.set_edits['2'][2] or self.set_edits['2'][2] == False:
                    if self.cap == 0 or max(window) <= float(self.cap):
                        if self.set_edits['5'][2]!= False and min(window)== self.set_edits['5'][2][0] and max(window)== self.set_edits['5'][2][1]:
                            self.manual_selection = self.winnum
                        self.save_window(window)
        if len(self.windex)==0:
            print "\n\nYour cap was set too low! either lower the window size or increase your cap.\n\n"
            self.cap = 0
            self.edit_mode()
        self.choiceDF = DataFrame.from_dict(self.windex,orient = 'index')
        self.choiceDF.columns = ["R2","Slope","Win Size","Win Start","Win End"]
        if self.set_edits['3'][2] == False or self.set_edits['3'][2] == '2':
            self.choiceDF.sort(['Slope','R2'], ascending = [0,0],inplace=True)
        else:
            self.choiceDF.sort(['R2','Slope'], ascending = [0,0],inplace=True)
        self.lnDF = self.xx.apply(np.log,0)

    def mkdir_p(self,path):
        try:
            os.makedirs(path)
        except OSError as exc: # Python >2.5
            if exc.errno == errno.EEXIST:
                pass
            else: raise
        return(path)
    
    def close_graph(self):
        if self.plotopen == True:
            self.send(None)
            self.plotopen = False
            time.sleep(0.5)

##############################################################################################################################################################################
        
class ProcessPlotter(object):

    def __init__(self):
        self.x = []
        self.y = []

    def terminate(self):
        plt.close('all')

    def poll_draw(self):
        #print("CCCC")
        def call_back():
            #print("BBB")
            while 1:
                if not self.pipe.poll():
                    break

                command = self.pipe.recv()
                #print("AAA")

                if command is None:
                    self.terminate()
                    return False

                else:
                    #print("AAAAAC")
                    pass
                    
            return True
        
        return call_back
    
    def singleplot(self):
        if self.logtime == False:
            self.window = 3
        else:
            self.window = 1
        getcontext().prec = 3
        fig = plt.figure(figsize=(8.0,8.0))
        self.axis_assign()
        self.ax = fig.add_subplot(111)
        self.create_plot(self.ax)
        xs,ys = self.fit_fn(self.yaxis.index, self.yaxis, self.temp_choice)
        self.ax.plot(xs,ys,'k-')
        self.keypos = self.set_key(self.ax)
        self.ax.text(self.keypos[0],self.keypos[1],"R2: {0}\ny={1}x + {2}".format(Decimal(self.intDict[self.temp_choice][self.window][1])+Decimal(0),
                                                                                            Decimal(self.intDict[self.temp_choice][self.window][0])+Decimal(0),
                                                                                             Decimal(self.intDict[self.temp_choice][self.window][2])+Decimal(0)),
                     color='k', fontsize = '10')
       
        if self.plot_type == 'single':
            self.ax2 = self.ax.twinx()
            self.logtime = False
            self.axis_assign()
            self.create_plot(self.ax2, colors=['g','y'])
            self.logtime = True
        if self.plot_type != 'single':
            self.ax.set_title(self.lnDF.columns[self.colnum])
            fig2 = plt.figure(figsize = (8.0,8.0))
            self.bx = fig2.add_subplot(111)
            self.bx.set_xlabel("Time(hours)")
            self.bx.set_ylabel(self.ylabels)
            if self.logtime == True:
                self.bx.plot(self.fullx,self.fully,'bo')
            else:
                self.bx.plot(self.fullx,self.fully,'go')
            self.bx.set_title(self.lnDF.columns[self.colnum])
            
            if self.logtime == False:
                fig.savefig('{0}_OD_WINDOW.pdf'.format(self.save_file,self.lnDF.columns[self.colnum]), format = 'pdf')
                fig2.savefig('{0}_OD.pdf'.format(self.save_file,self.lnDF.columns[self.colnum]), format = 'pdf')
                self.logtime = True
                self.singleplot()
            else:
                fig.savefig('{0}_LN_WINDOW.pdf'.format(self.save_file,self.lnDF.columns[self.colnum]), format = 'pdf')
                fig2.savefig('{0}_LN.pdf'.format(self.save_file,self.lnDF.columns[self.colnum]), format = 'pdf')

            
            
    def axis_assign(self):
        if self.plot_type == 'quad':
            if self.logtime == True:
                self.fully = self.lnDF[self.lnDF.columns[self.colnum]]
                self.fullx = self.lnDF.index
                self.yaxis = self.lnDF.ix[self.winList[self.window],self.colnum]
                self.ylimits = [self.lnDF.loc[min(self.winList[self.window])][self.colnum],self.lnDF.loc[max(self.winList[self.window])][self.colnum]]
                self.xlimits = [min(self.winList[self.window]),max(self.winList[self.window])]
                self.ylabels = 'Ln[OD]'
                return()
            else:
                self.fully = self.xx[self.xx.columns[self.colnum]]
                self.fullx = self.xx.index
                self.yaxis = self.xx.ix[self.winList[self.window],self.colnum]
                self.ylimits = [self.xx.loc[min(self.winList[self.window])][self.colnum],self.xx.loc[max(self.winList[self.window])][self.colnum]]
                self.xlimits = [min(self.winList[self.window]),max(self.winList[self.window])]
                self.ylabels = 'OD'
        else:
            if self.logtime == True:
                self.fully = self.lnDF[self.lnDF.columns[self.colnum]]
                self.fullx = self.lnDF.index
                self.yaxis = self.lnDF.ix[self.windowDict[self.temp_choice],self.colnum]
                self.ylimits = [self.lnDF.loc[min(self.windowDict[self.temp_choice])][self.colnum],self.lnDF.loc[max(self.windowDict[self.temp_choice])][self.colnum]]
                self.xlimits = [min(self.windowDict[self.temp_choice]),max(self.windowDict[self.temp_choice])]
                self.ylabels = 'Ln[OD]'
            else:
                self.fully = self.xx[self.xx.columns[self.colnum]]
                self.fullx = self.xx.index
                self.yaxis = self.xx.ix[self.windowDict[self.temp_choice],self.colnum]
                self.ylimits = [self.xx.loc[min(self.windowDict[self.temp_choice])][self.colnum],self.xx.loc[max(self.windowDict[self.temp_choice])][self.colnum]]
                self.xlimits = [min(self.windowDict[self.temp_choice]),max(self.windowDict[self.temp_choice])]
                self.ylabels = 'OD'
            
    def set_key(self,axes):
        ymin, ymax = axes.get_ylim()
        xmin, xmax = axes.get_xlim()
        if self.plot_type == 'final':
            xkey = xmin + (xmax-xmin)/40
            ykey = ymax - (ymax-ymin)/10
        else:
            xkey = xmin + (xmax-xmin)/45
            ykey = ymax - (ymax-ymin)/7
        keys = [xkey,ykey]
        return(keys)
    
    def create_plot(self,axes,colors=None):
        axes.set_xlabel('Time (h)')
        if colors != None:
            axes.set_ylabel(self.ylabels, color = colors[0])
            axes.plot(self.fullx, self.fully,'{0}o'.format(colors[0]))
            axes.plot(self.fullx, self.fully,'{0}-'.format(colors[0]))
            axes.plot(self.yaxis.index, self.yaxis,'{0}o'.format(colors[1]))
            axes.plot(self.yaxis.index, self.yaxis,'{0}-'.format(colors[1]))
        else:
            axes.set_ylabel(self.ylabels)
            if self.logtime == False:
                self.colorwheel = ['g','y']
            else:
                self.colorwheel = ['b','r']
            axes.plot(self.fullx, self.fully,'{0}o'.format(self.colorwheel[0]))
            axes.plot(self.fullx, self.fully,'{0}-'.format(self.colorwheel[0]))
            axes.plot(self.yaxis.index, self.yaxis,'{0}o'.format(self.colorwheel[1]))
            axes.plot(self.yaxis.index, self.yaxis,'{0}-'.format(self.colorwheel[1]))
        if colors == None and self.plot_type in ['quad','single']:
            if self.plot_type == 'quad':
                axes.set_title("Choice {0}".format(self.choiceDF.index[self.window]))
            elif self.plot_type == 'single':
                self.ax.set_title("Choice {0}".format(self.temp_choice))
        if colors != None:
            for tk in axes.get_yticklabels():
                tk.set_color(colors[0])
                tk.set_fontsize(10)
        elif self.plot_type in ['quad','single']:
            axes.set_ylabel(self.ylabels, color = 'b')
            for tk in axes.get_yticklabels():
                tk.set_color('b')
                tk.set_fontsize(10)
            for tk in axes.get_xticklabels():
                tk.set_fontsize(10)
        if self.keypos == None:
            self.keypos = self.set_key(self.ax)
        if self.logtime == True and self.plot_type == 'quad':
            xs,ys = self.fit_fn(self.yaxis.index, self.yaxis,2)
            axes.plot(xs,ys,'k-')
            axes.text(self.keypos[0],self.keypos[1],"R2: {0}\ny={1}x + {2}".format(Decimal(self.intDict[self.choiceDF.index[self.window]][1][1])+Decimal(0),
                                                                                                    Decimal(self.intDict[self.choiceDF.index[self.window]][1][0])+Decimal(0),
                                                                                                     Decimal(self.intDict[self.choiceDF.index[self.window]][1][2])+Decimal(0)),
                      color='k', fontsize = '8')
            
    def quad_plot(self):
        getcontext().prec = 3
        self.index = 0
        self.window = 0
        self.axis_assign()
        self.fig = plt.figure(figsize=(8.0,8.0))
        
        
        self.ax = self.fig.add_subplot(221)
        self.create_plot(self.ax)
        self.ax2 = self.ax.twinx()
        self.logtime = False
        self.axis_assign()
        self.create_plot(self.ax2, colors=['g','y'])
        self.logtime = True
        self.window += 1
        self.axis_assign()
        
        self.bx = self.fig.add_subplot(222)
        self.create_plot(self.bx)
        self.bx2 = self.bx.twinx()
        self.logtime = False
        self.axis_assign()
        self.create_plot(self.bx2, colors=['g','y'])
        self.logtime = True
        self.window += 1
        self.axis_assign()
        
        self.cx = self.fig.add_subplot(223)
        self.create_plot(self.cx)
        self.cx2 = self.cx.twinx()
        self.logtime = False
        self.axis_assign()
        self.create_plot(self.cx2, colors=['g','y'])
        self.logtime = True
        self.window += 1
        self.axis_assign()
        
        self.dx = self.fig.add_subplot(224)
        self.create_plot(self.dx)
        self.dx2 = self.dx.twinx()
        self.logtime = False
        self.axis_assign()
        self.create_plot(self.dx2, colors=['g','y'])
        self.logtime = True
        self.window += 1
        self.fig.tight_layout()
        
    
        
    def fit_fn(self,x,y,i):
        coeff = np.polyfit(x,y,1)
        poly = np.poly1d(coeff)
        xs = []
        if self.logtime == False:
            for x in self.xx.index:
                if poly(x) <= max(self.xx[self.xx.columns[self.colnum]]):
                    xs.append(x)
            ys = poly(xs)
            xs = np.array(xs)
            ys = np.array(ys)
            catch = [xs,ys]
        else:
            for x in self.lnDF.index:
                if poly(x) <= max(self.lnDF[self.lnDF.columns[self.colnum]]):
                    xs.append(x)
            ys = poly(xs)
            xs = np.array(xs)
            ys = np.array(ys)
            catch = [xs,ys]
        return catch
    
    def __call__(self, pipe, plot_type, lnDF, winList, colnum, windowDict, temp_choice, choiceDF, logtime, xx,intDict,save_file,bypass,examine):
        self.bypasslevel = bypass
        self.keypos = None
        self.save_file = save_file
        self.intDict = intDict
        self.logtime = logtime
        self.xx = xx
        self.choiceDF = choiceDF
        self.lnDF = lnDF
        self.winList = winList
        self.colnum = colnum
        self.plot_type = plot_type
        self.windowDict = windowDict
        self.temp_choice = temp_choice
        self.pipe = pipe
        self.gid = gobject.timeout_add(1000, self.poll_draw())
        if self.plot_type in ['single','final']:
            self.singleplot()
            if self.plot_type == 'final':
                plt.close('all')
        else:
            self.quad_plot()
            self.fig.savefig('{0}_Top4.pdf'.format(self.save_file,self.lnDF.columns[self.colnum]), format = 'pdf')
        if self.bypasslevel[2] != True and self.bypasslevel[1] != True and self.bypasslevel[3] != True:
            if self.bypasslevel[0] == True and examine == False and self.plot_type in ['single']:
                pass
            elif self.plot_type in ['quad','single']:
                thismanager = get_current_fig_manager()
                thismanager.window.move(0, 0)
                plt.show()
            elif self.bypasslevel[0] == True and self.plot_type == 'quad':
                thismanager = get_current_fig_manager()
                thismanager.window.move(0, 0)
                plt.show()
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Calculate specific growth rate and lag time for OD time series.")
    parser.add_argument("--quick", help='invoke to skip verification graphing and selection step.',
                        action = 'store_true')
    parser.add_argument("--auto", help='invoke to override user selection & skip display of graphs, will take top hit for all columns',
                        action = 'store_true')
    parser.add_argument("--noplot", help = "invoke to skip displaying graphs, graphs will still be saved.",
                        action = 'store_true')
    parser.add_argument("--nosave", help = "invoke to skip saving & display of graphs, CSV files will still be saved.",
                        action = 'store_true')
    parser.add_argument("-o", metavar = "OUTPUT", help = "Path to output folder where files will be placed. WILL OVERRIDE files with the same name in the directory.")
    parser.add_argument("-winrange", help = "Minimum then Maximum window size, separated by a space. Default = 6 15.",
                        nargs = 2, type = int, default = [6,15])
    parser.add_argument("-skiptocol", help = "Skip to a certain column in the datasheet.",
                        default = 0,type = int)
    parser.add_argument("--hardcap", help = "Cap all searches at a value.",
                        default = 0,type = int)
    args = parser.parse_args()
    dialog = gtk.FileChooserDialog("Select Input CSV File..",
                               None,
                               gtk.FILE_CHOOSER_ACTION_OPEN,
                               (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                gtk.STOCK_OPEN, gtk.RESPONSE_OK))
    dialog.set_default_response(gtk.RESPONSE_OK)

    filter = gtk.FileFilter()
    filter.set_name("All files")
    filter.add_pattern("*")
    dialog.add_filter(filter)

    filter = gtk.FileFilter()
    filter.set_name("Images")
    filter.add_mime_type("image/png")
    filter.add_mime_type("image/jpeg")
    filter.add_mime_type("image/gif")
    filter.add_pattern("*.png")
    filter.add_pattern("*.jpg")
    filter.add_pattern("*.gif")
    filter.add_pattern("*.tif")
    filter.add_pattern("*.xpm")
    dialog.add_filter(filter)

    response = dialog.run()
    if response == gtk.RESPONSE_OK:
        input_file = dialog.get_filename()
    elif response == gtk.RESPONSE_CANCEL:
        raise ValueError('Closed, no files selected')
    dialog.destroy()
    print args.hardcap
    spekit(args.o,input_file,args.winrange,args.quick,args.auto,args.noplot,args.nosave,args.skiptocol,args.hardcap)
    raw_input("Run completed with no errors. Press Enter to finish!")
