*** SPEKIT ***
Author: Andrew Swafford
Contact: Andrew.swafford@lifesci.ucsb.edu
Version: 0.2.2

BETA WARNING: This software is in the beta stages of development. It is strongly advised that all results are double-checked.

KNWON ISSUES:
Locking multiple items then attempting to clear will generate errors - please exercise restraint when locking items.

SUPPORTED PLATFORMS: 
Windows 7. All other platforms are untested.

###############   ###############   ###############  ###############   ###############   ###############   ###############


PURPOSE:
   The goal of spekit is to analyze a time series of optical density readings and accurately identify
the beginning of exponential growth phase. Currently, this is done by doing a basic ln tranformation of
the data and using a sliding window to identify the region with the highest (A) Slope and (B) R2 value.
These metrics perform resasonably well, though refinement of the model is clearly necessary before full 
automization can be relied upon. Once the region is identified by Spekit and verified by the user,
specific growth rate and lag time for that time series are calculated and stored for later access.

REQUIREMENTS:
  1|  Windows 7 (untested on all other platforms)
  2|  Python 2.7.X
     2a|  Matplotlib	-	pip install -U matplotlib
     2b|  Numpy  	- 	pip install -U Numpy
     2c|  Scipy  	- 	pip install -U scipy
     2d|  pandas 	- 	pip install -U pandas
     2e|  pygtk  	- 	http://ftp.gnome.org/pub/GNOME/binaries/win32/pygtk/2.22/pygtk-all-in-one-2.22.6.win32-py2.7.msi

USAGE:
in command line:
python spekit.py -i INPUT [options]

OPTIONS:
 * = Required
  1|*	-i INPUT	Path to input file.
  2|	-o OUTPUT	Path to output DIRECTORY, files will be named automatically
				DEFAULT: Creates folder in directory with input file
  3|	--quick		Invoke to skip verification graphing and selection step.
  4|    --auto		Invoke to override user selection & skip display of graphs, will take top hit for all columns.
  5|	--noplot	Invoke to skip displaying graphs, graphs will still be saved.
  6|    --nosave	Invoke to skip saving & display of graphs, CSV files will still be saved.
  7|	-h --help	Get help in command line interface.

INPUTS:
  1| CSV file with commas as separators and unique headers on every column. Each column shoud represent
	a single time series. Outputs will be named based on column header - it is advised that you make
	them informative.

OUTPUTS:
  1|	Full_output.csv		CSV file with statistics for each time series including specific growth rate and lag time.
  2|	XX_choices.csv		CSV file with the top 20 windows for the time series. One file will be created
					per time series.
  3|	XX_LN.pdf		PDF graph of the Ln-transformed time series.
  4|	XX_LN_WINDOW.pdf	PDF graph of the Ln-transformed time series with the selected window ad best fit line shown
  5|	XX_OD.pdf		PDF graph of the time series.
  6|    XX_OD_WINDOW.pdf	PDF graph of the time series with the selected window and best fit line shown. 
  7|	XX_Top4.pdf		PDF graph of the top 4 hits retrieved by the algorithm.
  8|	COMMANDS.txt		Text file of the command used to produce the outputs within the file.